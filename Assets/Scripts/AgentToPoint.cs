﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentToPoint : MonoBehaviour {

    public GameObject target;
    private NavMeshAgent navAgent;
    public bool followTarget, followFromStart;

	// Use this for initialization
	void Start () {
        navAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        if (target && followTarget)
        {
            navAgent.SetDestination(target.transform.position);
        }
	}
	
	// Update is called once per frame
	void Update () {
		if (target && followTarget)
        {
            navAgent.SetDestination(target.transform.position);
        }
	}
}
