﻿using UnityEngine;

/// <summary>
/// HeadBobber from: http://wiki.unity3d.com/index.php/Headbobber
/// Modifications: Translation from JS to C# and SmoothDamp movement options added
/// </summary>

namespace HeadBobbingSystem {

    [System.Serializable]
    public struct HeadBobberData {
        public float bobbingSpeed;
        public float bobbingAmount;
        public float midpoint;
        public bool smooth;
        public float smoothTime;
        [Range(0f, 1f)]
        public float idleBobbing;

        public HeadBobberData(float bobbingSpeed, float bobbingAmount, float midpoint, bool smooth, float smoothTime, float idleBobbing) {
            this.bobbingSpeed = bobbingSpeed;
            this.bobbingAmount = bobbingAmount;
            this.midpoint = midpoint;
            this.smooth = smooth;
            this.smoothTime = smoothTime;
            this.idleBobbing = idleBobbing;
        }
    }

    public class HeadBobber : MonoBehaviour {

        private float timer = 0.0f;
        private Vector3 smoothVelocity = Vector3.zero;
        private Vector3 targetPos;

        public HeadBobberData hbData = new HeadBobberData(0.18f, 0.2f, 2.0f, true, 0.3F, 0.1f);
        private float currentBobbing;

        private void Awake() {
            currentBobbing = hbData.idleBobbing;
        }

        void Update() {
            float waveslice = 0.0f;
           // float horizontal = Input.GetAxisRaw("Horizontal");
           // float vertical = Input.GetAxisRaw("Vertical");

            if (/*Mathf.Abs(horizontal) == 0 && Mathf.Abs(vertical) == 0*/ currentBobbing == 0) {
                timer = 0.0f;
            }
            else {
                waveslice = Mathf.Sin(timer);
                timer = timer + hbData.bobbingSpeed;

                if (timer > Mathf.PI * 2f) {
                    timer = timer - (Mathf.PI * 2f);
                }
            }

            if (waveslice != 0f) {
                float translateChange = waveslice * hbData.bobbingAmount;
                //float totalAxes = Mathf.Abs(horizontal) + Mathf.Abs(vertical);
                //totalAxes = Mathf.Clamp(totalAxes, 0f, 1.0f);
                translateChange = currentBobbing * translateChange;
                targetPos.Set(transform.localPosition.x, hbData.midpoint + translateChange, transform.localPosition.z);
            }
            else {
                targetPos.Set(transform.localPosition.x, hbData.midpoint, transform.localPosition.z);
            }

            if (hbData.smooth) {
                transform.localPosition = Vector3.SmoothDamp(transform.localPosition, targetPos, ref smoothVelocity, hbData.smoothTime);
            }
            else {
                transform.localPosition = targetPos;
            }
        }


        public void setBobbing(float bobbingAmount) {
            bobbingAmount = Mathf.Clamp01(bobbingAmount);
            currentBobbing = (bobbingAmount < hbData.idleBobbing) ? hbData.idleBobbing : bobbingAmount;
        }


        public void setConfig(HeadBobberData hbConfig) {
            hbData = hbConfig;
        }
    }
}

